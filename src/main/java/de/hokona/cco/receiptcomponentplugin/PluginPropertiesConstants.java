package de.hokona.cco.receiptcomponentplugin;

/**
 * robertzieschang 06.03.21
 **/
public final class PluginPropertiesConstants {

    // global plugin info
    public static final String PLUGIN_ID = "ReceiptComponentPlugin";
    public static final String PLUGIN_NAME = "A plugin to modify the receipt component";
}
