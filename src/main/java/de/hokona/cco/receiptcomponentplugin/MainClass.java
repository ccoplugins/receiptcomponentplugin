package de.hokona.cco.receiptcomponentplugin;

import com.sap.scco.ap.plugin.BasePlugin;
import com.sap.scco.ap.plugin.annotation.ui.JSInject;
import com.sap.scco.util.logging.Logger;

import java.io.InputStream;

/**
 * @author robertzieschang 06.03.21
 **/
public class MainClass extends BasePlugin {
    private static final Logger logger = Logger.getLogger(MainClass.class);

    @Override
    public String getId() {
        return PluginPropertiesConstants.PLUGIN_ID;
    }

    @Override
    public String getName() {
        return PluginPropertiesConstants.PLUGIN_NAME;
    }

    @Override
    public String getVersion() {
        return getClass().getPackage().getImplementationVersion();
    }

    @JSInject(targetScreen = "NGUI")
    public InputStream[] jsInject() {
        logger.finest("Injecting NGUI Javascript code.");
        return new InputStream[]{this.getClass().getResourceAsStream("/resources/receiptcomponentplugin.js")};
    }
}
