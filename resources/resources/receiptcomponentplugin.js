
Plugin.ReceiptComponentPlugin = class ReceiptComponentPlugin {
    constructor(pluginService, eventBus) {
    	this.pluginService = pluginService;
        this.eventBus = eventBus;
    
	    this.pluginExits = {    
	    		'ReceiptComponent.getColumnConfigProvider': () => {
	    			
	    			const decimalTools = this.pluginService.getContextInstance('DecimalTools');
	    			const formatters = this.pluginService.getContextInstance('Formatters');
	    			const currencyTools = this.pluginService.getContextInstance('CurrencyTools');
	    			const fontIconFactory = this.pluginService.getContextInstance('FontIconFactory');
	    			
	    			return (receiptComponent) => {
	    			    const config = [{
	    			        text: 'ID',
	    			        width: '17%',
	    			        type: 'component',	    			        
	    			        field: 'material.externalID',
	    			        id: 'externalId',
	    			        noAutoFontSize: true,
	    			        component: (cellValue, salesItem) => {	    			        	
	    			        	return {
	    			        		'component': 'ContainerComponent',
	    			        		'props': {
	    			        			'margin': '5%',
	    			        			'content': {
	    			        				'component': 'ButtonComponent',
	    			        				'props': {	    			        			
	    	    			        			'content': cellValue,
	    	    			        			'btnPress': () => {
	    	    			        				alert(cellValue);
	    	    			        			},
	    	    			        			'class': 'rowButton'
	    	    			        		}
	    			        			}
	    			        		}	    			        			    			        	
	    			        	};	    			        	
	    			        }
	    			    },{
	    			        text: receiptComponent.TL('SALES_ITEM_QUANTITY'),
	    			        width: '15%',
	    			        type: 'decimal',
	    			        field: 'quantity',
	    			        id: 'quantity',
	    			        formatter: (cellValue, salesItem) => {
	    			            return cellValue === null ? null : formatters.decimal.formatType(cellValue, decimalTools.TYPE_QUANTITY);
	    			        },
	    			        editor: new cco.ColumnTextEditor(receiptComponent, 'quantity', receiptComponent.TL('ENTER_QUANTITY'), receiptComponent.globalInputStore, receiptComponent.eventBus, null, 'quantity', (model, column) => {
	    			            let salesItem = column.salesItem;
	
	    			            let fallbackInputModel = new cco.FallbackInputModel(column.text);
	
	    			            if (receiptComponent.validateQuantityForReturn && salesItem.status !== '1') {
	    			                if (salesItem.quantity <= 0) {
	    			                    fallbackInputModel.setValidator(new cco.MinMaxNumberInputValidator(-salesItem.orderedQuantity, -1));
	    			                } else if (salesItem.orderedQuantity > 0) {
	    			                    fallbackInputModel.setValidator(new cco.MinMaxNumberInputValidator(salesItem.orderedQuantity));
	    			                }
	    			            }
	    			            return fallbackInputModel;
	    			        }, (column) => {
	    			            return !column['salesItem']['voucher'] && !column['salesItem']['immutable'];
	    			        })
	    			    }, {
	    			        text: receiptComponent.TL('MATERIAL'),
	    			        width: 'auto',
	    			        type: 'text',
	    			        class: (value, salesItem) => {
	    			        	return salesItem.material && salesItem.material.discountable ? 'discountableHighlight' : '';
	    			        },
	    			        field: 'description',
	    			        id: 'description',
	    			        editor: new cco.ColumnTextEditor(receiptComponent, 'description', receiptComponent.TL('DESCRIPTION'), receiptComponent.globalInputStore, receiptComponent.eventBus, null, 'description', (model, column) => {
	    			            let salesItem = column.salesItem;
	
	    			            let fallbackInputModel = new cco.FallbackInputModel(column.text);	    			            
	    			            return fallbackInputModel;
	    			        }, (column) => {
	    			            return !column['salesItem']['voucher'] && !column['salesItem']['immutable'];
	    			        }),
	    			        hints: true
	    			    }, {
							text: 'Unitprice',
							width: '10%',
							type: 'text',
							field: 'unitGrossAmount',
							id: 'unitGrossAmount',
							formatter: (cellValue, salesItem) => {
								return cellValue === null ? null : formatters.decimal.format(cellValue, currencyTools.getHomeCurrencyDecimals());
							},
							editor: new cco.ColumnPriceEditor(receiptComponent, 'unitGrossAmount', receiptComponent.TL('ENTER_PRICE'), receiptComponent.globalInputStore, receiptComponent.eventBus)
						},{
	    			        text: receiptComponent.TL('SALES_ITEM_DISCOUNT'),
	    			        width: '17%',
	    			        type: 'decimal',
	    			        field: 'discountAmount',
	    			        id: 'discount',
	    			        formatter: (cellValue, salesItem) => {
	    			            return cellValue === null ? null : formatters.decimal.formatType(-cellValue, decimalTools.TYPE_DISCOUNT_ABS);
	    			        },
	    			        editor: new cco.DiscountValueEditor(receiptComponent, receiptComponent.globalInputStore, receiptComponent.eventBus)
	    			    }, {
	    			        text: receiptComponent.TL('PRICE'),
	    			        width: '20%',
	    			        type: 'decimal',
	    			        id: 'price',
	    			        field: 'paymentGrossAmountWithoutReceiptDiscount',
	    			        formatter: (cellValue, salesItem) => {
	    			            return cellValue === null ? null : formatters.decimal.format(cellValue, currencyTools.getHomeCurrencyDecimals());
	    			        },
	    			        editor: new cco.ColumnPriceEditor(receiptComponent, 'unitGrossAmount', receiptComponent.TL('ENTER_PRICE'), receiptComponent.globalInputStore, receiptComponent.eventBus)
	    			    }
	    			    ];
	
	    			    return config;
	    			};
	    		},
	    		'ReceiptComponent.getRowModelCustomizer': () => {
	    			return (row, salesItem) => {	    			
	    				const fontIconFactory = this.pluginService.getContextInstance('FontIconFactory');
	    				
	    				if(salesItem.material && salesItem.material.discountable) {
	    					row.icon = fontIconFactory.getIcon(0xe808);
	    				}	    				
	    			}
	    		}
	    };
    }
  
};
